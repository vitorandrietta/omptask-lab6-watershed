# OpenMP -task- Labs

Let's learn parallel programming using OpenMP!

## Watershed

Watershed is a famous operator of image segmentation ; The image is seen as a surface being flooded as water springs from its regional minimum.
More informations see the folder 'theory'.

#### TODO:

1. Fork the repository to your own account.
2. Clone your forked repository on your computer.
3. Study the code using your favorite editor.
4. Download some dependences (if necessary) :

    $ sudo apt-get install liblapack-dev -y ; sudo apt-get install liblapack3 -y ; sudo apt-get install libopenblas-base -y ; sudo apt-get install libopenblas-dev -y ;

    $ sudo apt-get install libatlas-base-dev

5. Compile and run the program using these comands:

    $ make clean
    $ make watershed
    $ ./watershed.sh

6. Verify the images wrist-a.png and wrist-b.png that were created ,they will be your output.
7. Parallelize the code using OpenMP (add pragmas).
8. Compile the code again using the same comands.
9. Compare performance between serial and parallel version, remember to make sure that wrist-a.png and 
wrist-b.png are correct. 

Anything missing? Ideas for improvements? Make a pull request.
